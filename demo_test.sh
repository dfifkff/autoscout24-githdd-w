#!/bin/bash

# Image and model names
TEST_IMG=input/motorbike-race
MODEL_PATH=ckpt/ade20k-hrnetv2-c1
RESULT_PATH=./result

ENCODER=$MODEL_PATH/encoder_epoch_30.pth
DECODER=$MODEL_PATH/decoder_epoch_30.pth


# Inference
python3 -u demo.py \
  --imgs $TEST_IMG \
  --cfg config/ade20k-hrnetv2.yaml \
  DIR $MODEL_PATH \
  TEST.result ./result \
  TEST.checkpoint epoch_30.pth
